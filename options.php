<div class="wrap">
<?php screen_icon(); ?>
<h2>Levado Job Sharing</h2>
<p>Die folgenden Parameter werden auf allen Shortcodes angewendet, Sie können diese für jeden Shortcode überschreiben.</p>
<form method="post" action="options.php">
	<?php settings_fields('levado-job-group'); do_settings_fields('levado-job-group', 'levado-job-take'); ?>
	<table class="form-table">
		<tr valign="top">
			<th scope="row">Url zum Feed</th>
			<td>
				<input type="text" name="levado_job_from" value="<?php echo get_option('levado_job_from'); ?>">
				<p class="description">Hängen Sie an den Link zum gewünschten Markt <code>vacancy/feed</code> an. Zum Beispiel: <code>http://augsburgerjobs.de/vacancy/feed</code></p>
			</td>
		</tr>
		<tr valign="top">
			<th scope="row">Anzahl der Jobs</th>
			<td>
				<input type="text" name="levado_job_take" value="<?php echo get_option('levado_job_take'); ?>">
				<p class="description">Wie viele Jobs sollen in der Liste erscheinen?</p>
			</td>
		</tr>
		<tr valign="top">
			<th scope="row">Suchbegriff</th>
			<td>
				<input type="text" name="levado_job_query" value="<?php echo get_option('levado_job_query'); ?>">
				<p class="description">Stellenanzeigen passend zu einem bestimmten Stichwort, Jobtitel oder Unternehmen ausgeben.</p>
			</td>
		</tr>
		<tr valign="top">
			<th scope="row">Wohnort</th>
			<td>
				<input type="text" name="levado_job_location" value="<?php echo get_option('levado_job_location'); ?>">
				<p class="description">Die gewünschte Region innerhalb des Marktes.</p>
			</td>
		</tr>
		<tr valign="top">
			<th scope="row">Umkreis</th>
			<td>
				<input type="text" name="levado_job_radius" value="<?php echo get_option('levado_job_radius'); ?>"><span> km</span>
			</td>
		</tr>
		<tr valign="top">
			<th scope="row">Nur Top Jobs anzeigen</th>
			<td>
				<input type="checkbox" name="levado_job_promoted" value="1" <?php checked( 1 == get_option('levado_job_promoted') ); ?>>
			</td>
		</tr>
		<tr valign="top">
			<th scope="row">Art der Anzeige</th>
			<td>
				<select name="levado_ad_type" style="width: 170px">
					<option value="">Alle</option>
					<option value="1" <?php echo get_option('levado_ad_type') == 1 ? 'selected="selected"' : ''; ?>>Stellenanzeigen</option>
					<option value="2" <?php echo get_option('levado_ad_type') == 2 ? 'selected="selected"' : ''; ?>>Minijobs und Jobs als Aushilfe</option>
					<option value="3" <?php echo get_option('levado_ad_type') == 3 ? 'selected="selected"' : ''; ?>>Ausbildungsplätze</option>
					<option value="4" <?php echo get_option('levado_ad_type') == 4 ? 'selected="selected"' : ''; ?>>Praktikanten-, Werkstudenten und Abschlussarbeiten Jobs</option>
				</select>
			</td>
		</tr>
		<tr valign="top">
			<th scope="row">Arbeitgeber</th>
			<td>
				<input type="text" name="levado_job_company" value="<?php echo get_option('levado_job_company'); ?>">
				<p class="description">Hierbei handelt es sich um die Identifizierungsnummer des Unternehmens. Sie finden diese am Ende des Links zu Ihrem Unternehmensprofil. Zum Beispiel: <code>http://augsburgerjobs.de/unternehmen/musterfirma-21</code> Identifizierungsnummer: 21</p>
			</td>
		</tr>
		<tr valign="top">
			<th scope="row">Alternativtext</th>
			<td>
				<textarea name="levado_job_alt"><?php echo get_option('levado_job_alt'); ?></textarea><span>Falls keine Anzeigen vorhanden</span>
			</td>
		</tr>
		<tr valign="top">
			<th scope="row">Logo anzeigen</th>
			<td>
				<input type="checkbox" name="levado_job_show_logo" value="1" <?php checked( 1 == get_option('levado_job_show_logo') ); ?>>
			</td>
		</tr>
		<tr valign="top">
			<th scope="row">CSS</th>
			<td>
				<textarea name="levado_job_css"><?php echo get_option('levado_job_css'); ?></textarea><span>Manuell styles eintragen.</span>
			</td>
		</tr>
	</table>
	<?php submit_button(); ?>
</form><h3>Cache</h3>
<form method="post" action="<?php echo plugins_url(); ?>/levado-job-sharing/clear-cache.php">
	<p class="description">Die Stellenanzeigen werden für eine Stunde gespeichert. Hier können Sie diesen Speicher manuell leeren.</p>
	<?php submit_button('Cache Leeren'); ?>
</form>
<h3>Shortcode Dokumentation</h3>
<p>Sie können den Wordpress Shortcode in Artikel, Seiten und Widgets verwenden. Geben Sie dazu <code>[vacancy]</code> in die jeweilige Textbox ein. Um die Suche einzuschränken stehen Ihnen folgende Parameter zur Verfügung:</p>
<ul>
	<li><code>from</code>: Url zum Feed</li>
	<li><code>take</code>: Anzahl der Jobs</li>
	<li><code>query</code>: Suchbegriff</li>
	<li><code>location</code>: Wohnort</li>
	<li><code>radius</code>: Umkreis</li>
	<li><code>promoted</code>: Nur Top Jobs anzeigen</li>
	<li><code>company</code>: Arbeitgeber</li>
	<li><code>ad_type</code>: Art der Anzeige (1 = Stellenanzeigen, 2 = Minijobs und Jobs als Aushilfe, 3 = Ausbildungsplätze, 4 = Praktikanten-, Werkstudenten und Abschlussarbeiten Jobs)</li>
</ul>
<p><a href="http://codex.wordpress.org/Shortcode">Codex Seite zu shortcodes.</a></p>