<?php

if(isset($_POST['submit']))
{
	$files = glob(dirname(__FILE__) . '/cache/*');

	if (count($files) >= 1)
	{
		foreach($files as $file)
		{
			if(is_file($file))
			{
				@unlink($file);
			}
		}
	}

	?>
	<!doctype html>
	<html>
		<head>
			<title>Levado - Cache Leeren</title>
		</head>
		<body>
			Cache geleert. 
			<script>document.write('<a href="' + document.referrer + '">Back</a>');</script>
		</body>
	</html>
	<?php
}